package org.example;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Configurable
public class Application {

    /**
     * @description
     * @param
     * @param
     * @return
     * @return null
     **/

    public static void main(String[] args){
        SpringApplication.run(Application.class);
    }

}
